CFLAGS := -Os -std=gnu99
debug: CFLAGS := -Wall -Wextra -Werror -pedantic -pedantic-errors -std=gnu99 -g

all: csvsplit csvjoin
debug: csvsplit csvjoin
test: test-mac-csv \
      test-quotes-appropriately \
      test-quotes-unprintable \
      test-adds-newline \
      test-retains-newlines \
      test-ignores-blank-lines \
      test-accepts-staggered-rows

%: obj/%.o obj/writebuf.o
	$(CC) $^ -o $@

obj/%.o: src/%.c
	mkdir -p obj
	$(CC) -c $(CFLAGS) $< -o $@

.PHONY: test-mac-csv
test-mac-csv:
	@cat tests/$@.txt
	<tests/test-mac-csv-before.csv ./csvfix \
		| diff tests/test-mac-csv-after.csv -
	@echo

.PHONY: test-%
test-%: csvsplit csvjoin
	@cat tests/$@.txt
	<tests/$@-before.csv ./csvsplit \
		| ./csvjoin \
		| diff tests/$@-after.csv -
	@echo

.PHONY: benchmark-round-trip-performance
benchmark-round-trip-performance: tests/benchmark-round-trip-performance.csv csvsplit csvjoin
	@cat tests/$@.txt
	pv $^ \
		| ./csvsplit \
		| ./csvjoin \
		>/dev/null

# Creates a ~100MB test file. The file will be slightly less than 100MB after
# removing blank lines, but then we may add a final newline byte.
tests/benchmark-round-trip-performance.csv:
	(</dev/random tr -dc '[:lower:],\n' | head -c 100M; echo) \
		| sed -e '/^$$/d' >$@

.PHONY: clean
clean:
	rm -f csvsplit csvjoin obj/*.o
